import 'package:dhbt_okulu/widgets/score_calculation_textform.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TextFormRow extends StatelessWidget {
  late String text1;
  late String text2;
  TextFormRow({ Key? key , required this.text1, required this.text2}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 180,
          height: 120,
          padding: const EdgeInsets.all(8.0),
          child: TextForm(text: text1 ),
        ),
        Container(
          width: 180,
          height: 120,
          padding: const EdgeInsets.all(8.0),
          child: TextForm(text: text2),
        )
      ],
    );
  }
}