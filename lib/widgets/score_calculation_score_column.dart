// import 'package:dhbt_okulu/widgets/score_calculation_score.dart';
// import 'package:flutter/material.dart';

// class ScoreColumn extends StatefulWidget {
//   const ScoreColumn({ Key? key }) : super(key: key);

//   @override
//   _ScoreColumnState createState() => _ScoreColumnState();
// }

// class _ScoreColumnState extends State<ScoreColumn> {
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: [
//         Padding(
//           padding: const EdgeInsets.symmetric(vertical:8.0),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Score(
//                   titletext: "DHBT 1 NETİ",
//                   titlesize: 20,
//                   scoretext: 0.0,
//                   scoresize: 30,
//                 ),
//                 Score(
//                   titletext: "DHBT 2 NETİ",
//                   titlesize: 20,
//                   scoretext: 0.0,
//                   scoresize: 30,
//                 ),
//                 Score(
//                   titletext: "TOPLAM NET",
//                   titlesize: 20,
//                   scoretext: 0.0,
//                   scoresize: 30,
//                 ),
//               ],
//             ),
//         ),
//           Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Score(titletext: "KPSSDHBT PUANI", titlesize: 20, scoretext: 0.0, scoresize: 40),
//           )
//       ],
//     );
//   }
// }