// import 'package:dhbt_okulu/constants/color.dart';
// import 'package:dhbt_okulu/model/auth_model.dart';
// import 'package:flutter/material.dart';

// class LoginTexFormFiel extends StatefulWidget {
//   LoginTexFormFiel({Key? key}) : super(key: key);

//   @override
//   _LoginTexFormFielState createState() => _LoginTexFormFielState();
// }

// class _LoginTexFormFielState extends State<LoginTexFormFiel> {
//   final _emailRegExp = RegExp(
//       r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  
//   final formmodel = AuthFormModel();

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 20.0),
//       child: govdeKismiCenter(),
//     );
//   }

//   Center govdeKismiCenter() {
//     return Center(
//       child: Column(
//         children: [epostaTextFormField(), sifreTextFormField()],
//       ),
//     );
//   }

//   TextFormField epostaTextFormField() {
//     return TextFormField(
//       maxLength: 30,
//       validator: (value) {
//         if (value!.isEmpty) {
//           return "Lütfen bir e-posta adresi giriniz.";
//         } else if (!_emailRegExp.hasMatch(value)) {
//           return 'Geçersiz e-posta adresi!';
//         }
//         return null;
//       },
//       onSaved: (value) {
//         formmodel.emailAddress = value!;
//       },
//       onChanged: (value) {
//         setState(() {
//           formmodel.emailAddress = value;
//     print(formmodel.emailAddress);

//         });
//       },
//       keyboardType: TextInputType.emailAddress,
//       decoration: InputDecoration(
//           fillColor: Colors.transparent,
//           focusedBorder: UnderlineInputBorder(
//             borderSide: BorderSide(
//                 color: FixedColor().color3, style: BorderStyle.solid, width: 2),
//           ),
//           labelText: "E-Posta",
//           labelStyle: TextStyle(color: FixedColor().color2)),
//     );
//   }

//   TextFormField sifreTextFormField() {
//     return TextFormField(
//       maxLength: 30,
//       validator: (value) {
//         if (value!.isEmpty) {
//           return 'Lütfen şifrenizi giriniz.';
//         } else if (value.toString().length < 6) {
//           return 'Şifre 6 karakterden az olamaz.';
//         }
//         return null;
//       },
//       onSaved: (value) {
//         formmodel.password = value!;
//       },
//       onChanged: (value) {
//         setState(() {
//           formmodel.password = value;
//     print(" şifre -----------------------------> ${formmodel.password}");

//         });
//       },
//       obscureText: true,
//       decoration: InputDecoration(
//           fillColor: Colors.transparent,
//           focusedBorder: UnderlineInputBorder(
//             borderSide: BorderSide(
//                 color: FixedColor().color3, style: BorderStyle.solid, width: 2),
//           ),
//           labelText: "Sifre",
//           labelStyle: TextStyle(color: FixedColor().color2)),
//     );
//   }
// }
