import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/widgets/exam_ques_button.dart';
import 'package:flutter/material.dart';

class ExamQuesContainer extends StatefulWidget {
  late int quesnum;
  late String quesinform;
  late String ques;
  late String choicetext1;
  late String choicetext2;
  late String choicetext3;
  late String choicetext4;
  late String choicetext5;
  late String choicetext6;
  ExamQuesContainer({
    Key? key,
    required this.quesnum,
    this.quesinform="",
    required this.ques,
    required this.choicetext1,
    required this.choicetext2,
    required this.choicetext3,
    required this.choicetext4,
    required this.choicetext5,
    required this.choicetext6,
  }) : super(key: key);

  @override
  _ExamQuesContainerState createState() => _ExamQuesContainerState();
}

class _ExamQuesContainerState extends State<ExamQuesContainer> {
  @override
  Widget build(BuildContext context) {
    


    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
            padding: const EdgeInsetsDirectional.only(top: 15, start: 20, bottom: 15),
            child: Text(
              "Soru ${widget.quesnum}",
              style: TextStyle(color: FixedColor().color3, fontSize: 25),
            )),
        Divider(
          color: FixedColor().color3,
          height: 0,
          indent: 20,
          endIndent: 20,
          thickness: 2,
        ),
        Padding(
          padding: widget.quesinform==" " || widget.quesinform == "" ? const EdgeInsets.only(bottom: 0.0) : const EdgeInsets.all(20.0) ,
          child: Text(widget.quesinform),
        ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(widget.ques,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        ExamQuesButton(
          quesnum: widget.quesnum,
          choicenum: 1,
          choicetext: widget.choicetext1,
        ),
        ExamQuesButton(
          quesnum: widget.quesnum,
          choicenum: 2,
          choicetext: widget.choicetext2,
        ),
        ExamQuesButton(
          quesnum: widget.quesnum,
          choicenum: 3,
          choicetext: widget.choicetext3,
        ),
        ExamQuesButton(
          quesnum: widget.quesnum,
          choicenum: 4,
          choicetext: widget.choicetext4,
        ),
        ExamQuesButton(
          quesnum: widget.quesnum,
          choicenum: 5,
          choicetext: widget.choicetext5,
        ),
         ExamQuesButton(
          quesnum: widget.quesnum,
          choicenum: 6,
          choicetext: widget.choicetext6,
        ),
      ],
    );
  }
}

