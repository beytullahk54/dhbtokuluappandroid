import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/model/auth_user_values_provider.dart';
import 'package:flutter/material.dart';

class ResetTexFormFiel extends StatefulWidget {
  ResetTexFormFiel({Key? key}) : super(key: key);

  @override
  _ResetTexFormFielState createState() => _ResetTexFormFielState();
}

class _ResetTexFormFielState extends State<ResetTexFormFiel> {  
  @override
  Widget build(BuildContext context) {
    return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Center(
              child: Column(
                children: [
                  TextFormField(
                    maxLength: 30,
                    validator: (value){
                      if(value!.isEmpty){
                        print("emptmiş kanka");
                        return "burayı doldur";                    
                      }
                      print("dolu geldi kanka");
                      return null;
                    },
                    onChanged: (value){
                      setState(() {
                        authNotify.value.eposta = value;
                      });
                    },
                    decoration: InputDecoration(
                        fillColor: Colors.transparent,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: FixedColor().color3, style: BorderStyle.solid, width: 2),
                        ),
                        labelText: "E-Posta",
                        labelStyle: TextStyle(color: FixedColor().color2)
                        ),
                  ),
                ],
              ),
            ),
          );
  }
}