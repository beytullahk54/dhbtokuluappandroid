import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/model/score_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextForm extends StatelessWidget {
  late String text;
  TextForm({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: scoreNotify,
        builder: (BuildContext context, ScoreTextModel model, Widget? child) {
          return SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  text,
                  style: TextStyle(color: FixedColor().color2, fontSize: 20),
                ),
                TextFormField(
                  maxLength: text == "KPSS PUANI" ? 5 : 2,
                  style: const TextStyle(fontSize: 30),
                  keyboardType: TextInputType.number,
                  textAlign: TextAlign.center,
                  decoration: const InputDecoration(),
                  inputFormatters: text == "KPSS PUANI"
                      ? null
                      : <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                  onChanged: (value) {
                    if (value == "") {
                      scoreNotify.addScoreText(ScoreText(text, 0));
                    } else if (text == "KPSS PUANI") {
                      if (value.startsWith(".") &&
                          ".".allMatches(value).length < 2) {
                        String kpsspuan = "0" + value;
                        scoreNotify.addScoreText(
                            ScoreText(text, double.parse(kpsspuan)));
                      } else if (!value.contains('-') && !value.contains('.')) {
                        scoreNotify
                            .addScoreText(ScoreText(text, double.parse(value)));
                      }
                    } else {
                      scoreNotify
                          .addScoreText(ScoreText(text, int.parse(value)));
                    }
                  },
                  validator: validator(model),
                  onSaved: onSaved(),
                ),
              ],
            ),
          );
        });
  }

  validator(model) {
    if (text == "KPSS PUANI") {
      return kpssValidator(model);
    } else if (text == "DHBT 1 DOĞRU") {
      return dhbt1DValidator(model);
    } else if (text == "DHBT 1 YANLIŞ") {
      return dhbt1YValidator(model);
    } else if (text == "DHBT 2 DOĞRU") {
      return dhbt2DValidator(model);
    } else if (text == "DHBT 2 YANLIŞ") {
      return dhbt2YValidator(model);
    }
  }

  kpssValidator(model) {
    return (String? value) {
      if (value!.isEmpty) {
        return 'Lütfen bir değer giriniz.';
      } else if (value.contains('-') || ".".allMatches(value).length > 2) {
        return "Lütfen ondalıklı sayı veya tam sayı girin.";
      } else if (value.startsWith(".") && ".".allMatches(value).length < 2) {
        String kpsspuan = "0" + value;
        if (double.parse(kpsspuan) > 100) {
          return 'Kpss puanı en fazla 100 olabilir.';
        }
      } else if (!value.contains('-') && !value.contains('.')) {
        if (double.parse(value) > 100) {
          return 'Kpss puanı en fazla 100 olabilir.';
        }
      }
      return null;
    };
  }

  dhbt1DValidator(model) {
    return (value) {
      if (value.isEmpty) {
        return 'Lütfen bir değer giriniz.';
      } else if (int.parse(value) > 20) {
        return 'En fazla 20 olabilir.';
      }
      return null;
    };
  }

  dhbt1YValidator(model) {
    return (value) {
      if (value.isEmpty) {
        return 'Lütfen bir değer giriniz.';
      } else if (int.parse(value) > 20) {
        return 'En fazla 20 olabilir.';
      }
      return null;
    };
  }

  dhbt2DValidator(model) {
    return (value) {
      if (value.isEmpty) {
        return 'Lütfen bir değer giriniz.';
      } else if (int.parse(value) > 20) {
        return 'En fazla 20 olabilir.';
      }
      return null;
    };
  }

  dhbt2YValidator(model) {
    return (value) {
      if (value.isEmpty) {
        return 'Lütfen bir değer giriniz.';
      } else if (int.parse(value) > 20) {
        return 'En fazla 20 olabilir.';
      }
      return null;
    };
  }

  onSaved() {
    return (String? value) {
      if (text == "KPSS PUANI") {
        List kacnokta = value!.split(".");
        if (!value.contains(".")) {
          scoreNotify.addScoreText(ScoreText(text, double.parse(value)));
        } else {
          if (value[0] == "." && kacnokta.length < 3) {
            String kpsspuan = "0" + value;
            scoreNotify.addScoreText(ScoreText(text, double.parse(kpsspuan)));
          } else if (kacnokta.length < 3 && kacnokta[1] != "") {
            scoreNotify.addScoreText(ScoreText(text, double.parse(value)));
          }
        }
      } else {
        scoreNotify.addScoreText(ScoreText(text, double.parse(value!)));
      }
    };
  }
}
