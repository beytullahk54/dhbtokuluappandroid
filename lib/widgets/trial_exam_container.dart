import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbt_okulu/screen/trial_exam_ques.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class TrialExamContainer extends StatelessWidget {
  late bool kurumMu;
  late double height;
  late double width;
  late String containertext;
  late String whichExam;
  late String buttontext;
  late String subtitletext;
  TrialExamContainer(
      {Key? key,
      required this.kurumMu,
      required this.height,
      required this.width,
      required this.containertext,
      required this.subtitletext,
      required this.whichExam,
      required this.buttontext})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    late List<Color> myColor = [const Color(0xFF757575), const Color(0xFFBDBDBD)];
    if (kurumMu == true) {
      myColor = [Colors.green, Colors.greenAccent];
    }
    return Stack(
      children: [
        // Padding(
        //   padding: const EdgeInsets.all(20.0),
        //   child: Positioned(
        //         child: ClipPath(
        //           clipper: MyCustomClipper1(),
        //           child: Container(
        //     height: height,
        //     width: width,
        //     child: Center(child: Text(containertext)),
        //     decoration: BoxDecoration(
        //         borderRadius: BorderRadius.circular(20), color: Colors.blue,),
        //   ),
        //         ),
        //       ),

        // ),
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Container(
            height: height,
            width: width,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  containertext,
                  style: const TextStyle(fontSize: 16.0),
                ),
                Text(
                  "Sınavı Yapan Kurum:$subtitletext",
                  style: const TextStyle(fontSize: 12.0),
                )
              ],
            )),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(colors: myColor)),
          ),
        ),

        Padding(
          padding: EdgeInsets.fromLTRB(MediaQuery.of(context).size.width * 0.29,
              MediaQuery.of(context).size.height * 0.19, 0, 5.0),
          child: ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            TrialExamQues(whichTrialExam: whichExam)));
              },
              child: Text(buttontext),
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50)),
                fixedSize: Size(MediaQuery.of(context).size.width * 0.4,
                    MediaQuery.of(context).size.height * 0.05),
              )),
        ),
      ],
    );
  }
}

// class MyCustomClipper1 extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     Path path = Path();
//     path.lineTo(0, size.height - 100);
//     path.quadraticBezierTo(
//         size.width / 2, size.height, size.width, size.height - 100);
//     path.lineTo(size.width, 0);
//     path.close();

//     return path;
//   }

//   @override
//   bool shouldReclip(MyCustomClipper1 oldClipper) => false;
// }

// class MyCustomClipper2 extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     Path path = Path();
//     path.lineTo(0, size.height-100);
//     // path.quadraticBezierTo(
//     //     size.width / 2, size.height-100, size.width, size.height);
//     path.relativeQuadraticBezierTo(size.width /2, size.height-100, size.width, size.height);
//     path.lineTo(size.width,0 );
//     path.close();

//     return path;
//   }

//   @override
//   bool shouldReclip(MyCustomClipper2 oldClipper) => false;
// }