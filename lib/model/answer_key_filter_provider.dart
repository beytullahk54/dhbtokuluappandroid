
import 'package:flutter/material.dart';

final answerChoiceNotify = AnswerChoiceNotify(AnswerKeyFilterChoice());

class AnswerChoiceNotify extends ValueNotifier<AnswerKeyFilterChoice>  {
  AnswerChoiceNotify(AnswerKeyFilterChoice value) : super(value);
  
  void onItemTapped(int index){
    value.selectedIndex = index;
    notifyListeners();
  }
}

class AnswerKeyFilterChoice {
  int? selectedIndex;
}