import 'package:cloud_firestore/cloud_firestore.dart';

class DuyuruModel {
  final String id;
  final String baslik;
  final String metin;
  final Timestamp tarih;

  DuyuruModel(
      {required this.id,
      required this.baslik,
      required this.metin,
      required this.tarih });

  factory DuyuruModel.fromMap(Map<String, dynamic> data, String documentId) {
    // if (data == null) {
    //   return null;
    // }

    String baslik = data['baslik'];
    String metin = data['metin'];
    Timestamp tarih = data['tarih'];

    return DuyuruModel(
        id: documentId, baslik: baslik, metin: metin, tarih: tarih );
  }

  Map<String, dynamic> toMap() {
    return {
      'baslik': baslik,
      'metin': metin,
      'tarih': tarih
    };
  }
}
