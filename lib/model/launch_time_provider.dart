import 'package:flutter/material.dart';

final launchNotify = LaunchNotify(LaunchValuesModel());

class LaunchNotify extends ValueNotifier<LaunchValuesModel>  {
  LaunchNotify(LaunchValuesModel value) : super(value);
  
    void changeLaunchTimeValues(DateTime time){      
      value.time = time;
      notifyListeners();
  }  
}

class LaunchValuesModel {  
  DateTime? time;
  
}
