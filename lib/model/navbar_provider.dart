
import 'package:dhbt_okulu/screen/hap_bilgiler.dart';
import 'package:dhbt_okulu/screen/home_page.dart';
import 'package:dhbt_okulu/screen/profil_page.dart';
import 'package:flutter/material.dart';

final selectedNotify = SelectedNotify(ItemTapped());

class SelectedNotify extends ValueNotifier<ItemTapped>  {
  SelectedNotify(ItemTapped value) : super(value);
  
  void onItemTapped(int index){
    value.selectedIndex = index;
    notifyListeners();
  }

   Widget showPage() {
    if (value.selectedIndex == 0) {
      return const MyHomePage();
    }else if(value.selectedIndex==1){
      return const HapBilgiler();
    }
    return const Profil();
  }

}

class ItemTapped {
  late int selectedIndex = 0;

}