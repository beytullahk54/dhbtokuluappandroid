import 'package:flutter/material.dart';

final scoreNotify = ScoreNotify(ScoreTextModel());

class ScoreNotify extends ValueNotifier<ScoreTextModel>{

  ScoreNotify(ScoreTextModel value) : super(value);


  void addScoreText(ScoreText scoreText){
    value.scoreMap[scoreText.text]=scoreText.value;

    notifyListeners();
  }

}

class ScoreTextModel {
  late Map<String,dynamic> scoreMap={"KPSS PUANI":0,"DHBT 1 DOĞRU":0,"DHBT 1 YANLIŞ":0,"DHBT 2 DOĞRU":0,"DHBT 2 YANLIŞ":0};
}

class ScoreText {
  late String text;
  late var value;

  ScoreText(this.text,this.value);
}
