import 'package:flutter/material.dart';

class FixedColor {
  Color color1= Colors.white;
  Color? color2= Colors.grey[500];
  Color color3= const Color(0xff00bfa5);
  Color color4 = Colors.black;
  Color bodyColor = const Color(0xFF6F8398);
}