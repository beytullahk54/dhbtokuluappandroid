// hapbilgi
import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/model/hapbilgi_model.dart';
import 'package:dhbt_okulu/services/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// late int refreshNum = 12;

class HapBilgiler extends StatefulWidget {
  const HapBilgiler({Key? key}) : super(key: key);

  @override
  _HapBilgilerState createState() => _HapBilgilerState();
}

class _HapBilgilerState extends State<HapBilgiler>
    with SingleTickerProviderStateMixin {
  TabController? _controller;

  @override
  void initState() {
    // TODO: implement initStatef
    super.initState();
    _controller = TabController(length: 2, vsync: this, initialIndex: 0);
  }

  @override
  Widget build(BuildContext context) {
    final listenDocument =
        Provider.of<FirebaseDatabaseService>(context, listen: false);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: FixedColor().color3,
          automaticallyImplyLeading: false,
          toolbarHeight: 3,
          bottom: TabBar(
              controller: _controller,
              indicatorColor: FixedColor().color1,
              tabs: const [
                Tab(
                  icon: Icon(Icons.grid_on),
                ),
                Tab(
                  icon: Icon(Icons.format_line_spacing),
                )
              ]),
        ),
        body: StreamBuilder<List<dynamic>>(
            stream: listenDocument.roomsStream(),
            builder:
                (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
              if (snapshot.hasError) {
                return const Text(
                    'Veri AKışında Bir Hatayla Karşılaşıldı! \n Tekrar Deneyin!!!');
              }

              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Center(child: CircularProgressIndicator());
              }
              return TabBarView(
                controller: _controller,
                children: [
                  HapGrid(
                    snapshot: snapshot,
                  ),
                  HapListView(
                    snapshot: snapshot,
                  )
                ],
              );
            }));
  }
}

class HapGrid extends StatefulWidget {
  late AsyncSnapshot<List<dynamic>> snapshot;
  HapGrid({Key? key, required this.snapshot}) : super(key: key);

  @override
  _HapGridState createState() => _HapGridState();
}

class _HapGridState extends State<HapGrid> {
  // late ScrollController _scrollController;
  late String _uri =
      "https://www.dhbtokulu.com/wp-content/uploads/2021/09/dhbt-okulu-site-logo.png";
  late int SnapshotDataDocsLenIndex = 0;
  final ValueNotifier<int> elementnum = ValueNotifier<int>(0);

  // @override
  // void initState() {
  //   _scrollController = ScrollController();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    List<HapBilgiModel>? hapBilgiler =
        widget.snapshot.data!.cast<HapBilgiModel>();
    return GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemBuilder: (BuildContext context, int index) {
          _uri = hapBilgiler[hapBilgiler.length - 1 - index].imageUrl;
          return InkWell(
            child: Image.network(_uri),
            onTap: () {
              SnapshotDataDocsLenIndex = hapBilgiler.length - 1 - index;
              elementnum.value = hapBilgiler.length - 1 - index;
              createAlertDialog(context);
            },
          );
        },
        itemCount: hapBilgiler.length);
    // return CustomScrollView(
    //   slivers: [
    // SliverGrid(
    //   gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
    //       crossAxisCount: 3),
    //   delegate: SliverChildBuilderDelegate(//equivalent to Listview.builder
    //       (context, index) {
    //     _uri = hapBilgiler[hapBilgiler.length - 1 - index].imageUrl;
    //     return InkWell(
    //       child: Image.network(_uri),
    //       onTap: () {
    //         SnapshotDataDocsLenIndex = hapBilgiler.length - 1 - index;
    //         createAlertDialog(
    //             context, hapBilgiler[SnapshotDataDocsLenIndex].imageUrl);
    //       },
    //     );
    //   }, childCount: hapBilgiler.length),
    // ),
    //     SliverList(
    //       delegate: SliverChildBuilderDelegate((content, index) {
    //         return TextButton(
    //             onPressed: () {
    //               // refreshNum = refreshNum + 12;
    //               // refreshNotify.onRefresh(refreshNum);
    //               if (_scrollController.hasClients) {
    //                 _scrollController.jumpTo(index.toDouble());
    //               }
    //             },
    //             child: Text("Daha fazla görüntüle"));
    //       }, childCount: 1),
    //     )
    //   ],
    // );
  }

  createAlertDialog(BuildContext context) {
    late int _start = 0;
    late int _end = 0;

    int calc_ranks(ranks) {
      double multiplier = .5;
      return (multiplier * ranks).round();
    }

    List<HapBilgiModel>? hapBilgiler =
        widget.snapshot.data!.cast<HapBilgiModel>();

    return showDialog(
        //alert dialogun gözükmesini sağlar
        context: context,
        builder: (context) {
          return ValueListenableBuilder(
            valueListenable: elementnum,
            builder: (BuildContext context, int value, Widget? child) {
              return AlertDialog(
                //uyarı kutucuğunun çıkmasını sağlar
                insetPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                scrollable: true, // taşan nesnelerde kaydırma özelliği sağlar
                content: GestureDetector(
                  onVerticalDragStart: (event) {
                    _start = calc_ranks(event.globalPosition.dx);
                  },
                  onVerticalDragUpdate: (event) {
                    _end = calc_ranks(event.globalPosition.dx);
                  },
                  onVerticalDragEnd: (event) {
                    //sağaa dönderen event
                    if (_start - _end > 0) {
                      if (_start - _end > 60) {
                        // Navigator.pop(context);
                        if (value == 0) {
                          null;
                        } else {
                          elementnum.value = value - 1;
                        }
                      }
                      //sola dönderren event
                    } else if (_start - _end < 0) {
                      if (_start - _end < -60) {
                        // Navigator.pop(context);
                        if (value == hapBilgiler.length - 1) {
                          null;
                        } else {
                          elementnum.value = value + 1;
                        }
                      }
                    }
                    // print("start: ${_start}\n");
                    // print("end: ${_end}\n");
                  },
                  child: Column(
                    children: <Widget>[
                      Image.network(hapBilgiler[value].imageUrl),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          IconButton(
                            onPressed: () {
                              print(hapBilgiler[value].imageUrl);
                              if (value == hapBilgiler.length - 1) {
                                null;
                              } else {
                                elementnum.value = value + 1;
                              }
                            },
                            icon: const Icon(Icons.arrow_left_outlined),
                            color: const Color(0xff1e83c1),
                            iconSize: 60,
                          ),
                          IconButton(
                            onPressed: () {
                              if (value == 0) {
                                null;
                              } else {
                                elementnum.value = value - 1;
                              }
                            },
                            icon: const Icon(Icons.arrow_right_outlined),
                            color: const Color(0xff1e83c1),
                            iconSize: 60,
                          )
                        ],
                      )
                    ],
                  ), //uyarı kutucuğunun içindeki metin
                ),
                backgroundColor: Colors.transparent,
              );
            },
          );
        });
  }
}

class HapListView extends StatefulWidget {
  late AsyncSnapshot<List<dynamic>> snapshot;
  HapListView({Key? key, required this.snapshot}) : super(key: key);

  @override
  _HapListViewState createState() => _HapListViewState();
}

class _HapListViewState extends State<HapListView> {
  @override
  Widget build(BuildContext context) {
    List<HapBilgiModel>? hapBilgiler =
        widget.snapshot.data!.cast<HapBilgiModel>();

    return ListView.builder(
        shrinkWrap: true,
        itemCount: widget.snapshot.data!.length,
        itemBuilder: (BuildContext context, int index) {
          return Image.network(
              hapBilgiler[hapBilgiler.length - 1 - index].imageUrl);
        });
  }
}
