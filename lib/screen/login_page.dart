import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/model/launch_time_provider.dart';
import 'package:dhbt_okulu/screen/login.dart';
import 'package:dhbt_okulu/screen/register.dart';
import 'package:dhbt_okulu/widgets/getTimes.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    launchNotify.value.time = getTimes().getTheTimes();
    return Scaffold(
      body: govde(context),
    );
  }

   govde(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            const AspectRatio(

              // height: MediaQuery.of(context).size.height * 0.4,
              aspectRatio: 5/3,
              child: Image(image: AssetImage('images/dhbtLogo1.jpg')),
              // width: MediaQuery.of(context).size.width,
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height*0.03,
            ),
            Text(
              "Hedefine ulaşmak ister misin?",
              style: TextStyle(fontSize: 25, color: FixedColor().color2),
            ),
            kayitOlButonPadding(context),
            girisYapButon(context),
            SizedBox(
              height: MediaQuery.of(context).size.height*0.4,
              child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: AspectRatio(
                  aspectRatio: 4,
                  child: Image.asset("images/powered_by.png", height: 100.0,)),
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding kayitOlButonPadding(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: ElevatedButton(
        onPressed: () {
          Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.topToBottom,
                  duration: const Duration(seconds: 1),
                  child: const Register()));
        },
        child: const Text("Kayıt Ol", style: TextStyle(fontSize: 17)),
        style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
          primary: FixedColor().color3,
          fixedSize: Size(MediaQuery.of(context).size.width * 0.9,
              MediaQuery.of(context).size.height * 0.055),
        ),
      ),
    );
  }

  OutlinedButton girisYapButon(BuildContext context) {
    return OutlinedButton(
      child: Text(
        "Giriş Yap",
        style: TextStyle(color: FixedColor().color3, fontSize: 17),
      ),
      style: OutlinedButton.styleFrom(
          primary: Colors.white,
          side: BorderSide(color: FixedColor().color3, width: 1),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
          fixedSize: Size(MediaQuery.of(context).size.width * 0.9,
              MediaQuery.of(context).size.height * 0.055)),
      onPressed: () {
        Navigator.push(
            context,
            PageTransition(
                type: PageTransitionType.topToBottom,
                duration: const Duration(seconds: 1),
                child: const Login()));
      },
    );
  }

  
}
