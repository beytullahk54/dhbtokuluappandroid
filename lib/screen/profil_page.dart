import 'dart:io';
import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/model/launch_time_provider.dart';
import 'package:dhbt_okulu/screen/login_page.dart';
import 'package:dhbt_okulu/screen/reset_page.dart';
import 'package:dhbt_okulu/widgets/getTimes.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:page_transition/page_transition.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

class Profil extends StatefulWidget {
  const Profil({Key? key}) : super(key: key);

  @override
  _ProfilState createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {
  File? _imageFile = null;
  final picker = ImagePicker();

  int? totalTime;

  @override
  Widget build(BuildContext context) {
    DateTime finalTime = getTimes().getTheTimes();
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.8,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(250.0),
                      bottomRight: Radius.circular(10.0)),
                  gradient: LinearGradient(
                      colors: [Color(0xff00bfa5), Colors.grey],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight)),
            ),
            Container(
              margin: const EdgeInsets.only(top: 80),
              child: isimPading(context, finalTime),
            ),
          ],
        ),
      ),
    );
  }

  Column isimPading(BuildContext context, DateTime finalTime) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              "Sn.${Hive.box("logindata").get("isim").toString().toUpperCase()} ${Hive.box("logindata").get("soyisim").toString().toUpperCase()}",
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 28,
                  fontStyle: FontStyle.italic),
            ),
          ),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.02),
        calisarakGecirilenToplamSureText(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.009),
        circularWorkingTotalTimeProcess(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.009),
        calisarakGecirilenSureText(),
        SizedBox(height: MediaQuery.of(context).size.height * 0.009),
        circularWorkingTimeProcess(finalTime),
        SizedBox(height: MediaQuery.of(context).size.height * 0.03),
        OutlinedButton(
          child: Text(
            "Şifre Sıfırlama",
            style: TextStyle(color: FixedColor().color1),
          ),
          style: OutlinedButton.styleFrom(
            side: BorderSide(color: FixedColor().color1, width: 1),
          ),
          onPressed: () => Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.bottomToTop,
                  duration: const Duration(seconds: 1),
                  child: ResetPage(
                    appbarText: "Şifre Sıfırlama",
                  ))),
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.04),
        exitButonExpanded(context),
      ],
    );
  }

  Text calisarakGecirilenSureText() {
    return const Text(
      "Bizimle Geçirdiğiniz Şuanlık Süre",
      style: TextStyle(
          color: Colors.white, fontSize: 18, fontStyle: FontStyle.italic),
    );
  }

  Text calisarakGecirilenToplamSureText() {
    return const Text(
      "Bizimle Geçirdiğiniz Toplam Süre",
      style: TextStyle(
          color: Colors.white, fontSize: 18, fontStyle: FontStyle.italic),
    );
  }

  circularWorkingTimeProcess(DateTime finalTime) {
    return ValueListenableBuilder(
        valueListenable: launchNotify,
        builder:
            (BuildContext context, LaunchValuesModel value, Widget? child) {
          return CircularPercentIndicator(
            radius: 130.0,
            animation: true,
            animationDuration: 1200,
            lineWidth: 15.0,
            percent: 0.4,
            center: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(children: [
                  TextSpan(
                      text:
                          "${((finalTime.minute - value.time!.minute)).toInt()}\n",
                      style: const TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.black)),
                  const TextSpan(
                      text: "Dakika", style: TextStyle(color: Colors.black))
                ])),
            circularStrokeCap: CircularStrokeCap.butt,
            backgroundColor: Colors.blueGrey.shade300,
            progressColor: Colors.blue,
          );
        });
  }

  circularWorkingTotalTimeProcess() {
    totalTime = Hive.box("logindata").get("apptime");
    return CircularPercentIndicator(
      radius: 130.0,
      animation: true,
      animationDuration: 1200,
      lineWidth: 15.0,
      percent: 0.4,
      center: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(children: [
            TextSpan(
                text: totalTime == null ? "0\n" : "${(totalTime)}\n",
                style: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                    color: Colors.black)),
            const TextSpan(
                text: "Dakika", style: TextStyle(color: Colors.black))
          ])),
      circularStrokeCap: CircularStrokeCap.butt,
      backgroundColor: Colors.blueGrey.shade300,
      progressColor: Colors.blue,
    );
  }

  exitButonExpanded(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Stack(
            children: [
              Container(
                height: double.infinity,
                margin:
                    const EdgeInsets.only(left: 30.0, right: 30.0, top: 10.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: _imageFile != null
                      ? Image.file(_imageFile!)
                      : ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              fixedSize: const Size(100, 80),
                              primary: FixedColor().color3),
                          child: const Text(
                            "Çıkış Yap",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          onPressed: () async {
                            await FirebaseAuth.instance.signOut().then((value) {
                              Hive.box("logindata").delete("useruid");
                              Hive.box("logindata").delete("isim");
                              Hive.box("logindata").delete("soyisim");
                              Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      const LoginPage(),
                                ),
                                (route) => false,
                              );
                            });
                          },
                        ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
