// seminer2

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/widgets/youtube_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

class Seminars extends StatefulWidget {
  const Seminars({Key? key}) : super(key: key);

  @override
  _SeminarsState createState() => _SeminarsState();
}

class _SeminarsState extends State<Seminars> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
  }

  @override
  Widget build(BuildContext context) {
    final Stream<QuerySnapshot> _slideStream = FirebaseFirestore.instance
        .collection('seminer')
        .orderBy('tarih', descending: true)
        .snapshots();
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Seminerler",
            style: TextStyle(color: FixedColor().color3),
          ),
          centerTitle: true,
          backgroundColor: FixedColor().color1,
          shadowColor: FixedColor().color1,
          iconTheme: IconThemeData(
            color: FixedColor().color3,
          ),
        ),
        body: streamBuilderWidget(_slideStream));
  }

  StreamBuilder<QuerySnapshot<Object?>> streamBuilderWidget(
      Stream<QuerySnapshot<Object?>> _slideStream) {
    return StreamBuilder<QuerySnapshot>(
        stream: _slideStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text('Bir şeyler yanlış gitti.');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }
          return listviewBuilderPadding(snapshot);
        });
  }

  Padding listviewBuilderPadding(
      AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 0.0),
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: const BouncingScrollPhysics(),
        itemCount: snapshot.data!.docs.length,
        itemBuilder: (BuildContext context, int index) {
          return _buildPlayerModelList(snapshot, index);
        },
      ),
    );
  }

  Widget _buildPlayerModelList(
      AsyncSnapshot<QuerySnapshot<Object?>> snapshot, index) {
    var tarih = snapshot.data!.docs.toList()[index]['tarih'].toDate();
    final dateformat = DateFormat('dd-MM-yyyy HH:mm');
    DateTime dob = DateTime.parse(tarih.toString());
    Duration dur = DateTime.now().difference(dob);
    return expandedTileWidget(snapshot, index, dateformat, tarih, dur);
  }

  Card expandedTileWidget(AsyncSnapshot<QuerySnapshot<Object?>> snapshot, index,
      DateFormat dateformat, tarih, Duration dur) {
    return Card(
      child: ExpansionTile(
        backgroundColor: FixedColor().color1,
        collapsedIconColor: FixedColor().color1,
        collapsedTextColor: FixedColor().color1,
        collapsedBackgroundColor: FixedColor().color3,
        iconColor: FixedColor().color3,
        textColor: FixedColor().color3,
        title: expandedTileTitleColumn(snapshot, index, dateformat, tarih),
        children: <Widget>[
          expandedTileInBodyAndImage(snapshot, index, dur),
        ],
      ),
    );
  }

  Column expandedTileTitleColumn(AsyncSnapshot<QuerySnapshot<Object?>> snapshot,
      index, DateFormat dateformat, tarih) {
    return Column(
      children: [
        Text(
          snapshot.data!.docs.toList()[index]['baslik'],
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
        const Divider(
          color: Colors.white,
        ),
        Text(
          snapshot.data!.docs.toList()[index]['kurum'],
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        Text(
          // snapshot.data!.docs.toList()[index]['tarih'].toDate().toString(),
          dateformat.format(tarih),
          // dateformat.parse(snapshot.data!.docs.toList()[index]['tarih'].toString()).toString() ,
          style: const TextStyle(
            fontSize: 12.0,
            fontWeight: FontWeight.bold,
          ),
        )
      ],
    );
  }

  Column expandedTileInBodyAndImage(
      AsyncSnapshot<QuerySnapshot<Object?>> snapshot, index, Duration dur) {
    return Column(
      children: [

          
GestureDetector(
                onTap: dur.inSeconds >= 1
            ? () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => YoutubePlayerWidget(
                              videoUrl: snapshot.data!.docs.toList()[index]
                                  ['videoUrl'],
                            )));
              }
            : null,
                child: Image(
          image: NetworkImage(snapshot.data!.docs.toList()[index]['fotoUrl']),
                fit: BoxFit.cover,
                ),
            ),

        expandedTileInYoutubeButton(snapshot, index, dur)
      ],
    );
  }

  ElevatedButton expandedTileInYoutubeButton(
      AsyncSnapshot<QuerySnapshot<Object?>> snapshot, index, Duration dur) {
    return ElevatedButton(
        onPressed: dur.inSeconds >= 1
            ? () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => YoutubePlayerWidget(
                              videoUrl: snapshot.data!.docs.toList()[index]
                                  ['videoUrl'],
                            )));
              }
            : null,
        child: (dur.inSeconds >= 1)
            ? const Text("Seminer İzle")
            : const Text("İlk izleyen olmak için takipte kal"));
  }
}





// ListView.builder(
//           itemCount: 5,
//           itemBuilder: (BuildContext ctxt, int index) {
//             return GestureDetector(
//               child: Card(
//                 color: FixedColor().color3,
//                 child: Row(
//                   children: [
//                     SizedBox(
//                       width: MediaQuery.of(context).size.width * 0.33,
//                       child: Image.asset(
//                         "images/facebook.png",
//                         fit: BoxFit.fill,
//                       ),
//                     ),
//                     Flexible(
//                       child: Center(
//                         child: Column(
//                           children: [
//                             Text(
//                               "Sınava Hazırlık Semineri",
//                               style: TextStyle(
//                                   fontSize: 15.0, fontWeight: FontWeight.bold,color: FixedColor().color1),
//                             ),
//                             Text(
//                               "İstanbul Üniversitesi",
//                               style: TextStyle(
//                                 fontSize: 14.0,
//                                 fontWeight: FontWeight.normal,
//                                 color: FixedColor().color1
//                               ),
//                             ),
                            // Text(
                            //   'Population: 22.06.2020',
                            //   style: TextStyle(
                            //     fontSize: 12.0,
                            //     fontWeight: FontWeight.normal,
                            //     color: FixedColor().color1
                            //   ),
                            // ),
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               onTap: (){
//                 Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => const SeminarDetail()));
//               },
//             );
//           }),