// sınav isimlerini firebaseden çekeceğiz

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/model/duyuru_model.dart';
import 'package:dhbt_okulu/model/trial_exam_provider.dart';
import 'package:dhbt_okulu/screen/notification_page.dart';
import 'package:dhbt_okulu/services/firebase_database.dart';
import 'package:dhbt_okulu/widgets/trial_exam_container.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class TrialExam extends StatefulWidget {
  const TrialExam({Key? key}) : super(key: key);

  @override
  _TrialExamState createState() => _TrialExamState();
}

class _TrialExamState extends State<TrialExam> {
  final _firestore = FirebaseFirestore.instance;
  late String dbData = "bekleniyor..";

  @override
  Widget build(BuildContext context) {
    Future<QuerySnapshot<Map<String, dynamic>>> denemeRef = _firestore
        .collection("deneme")
        .orderBy("sira", descending: false)
        .get();

    final listenDocument =
            Provider.of<FirebaseDatabaseService>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Text(
              "Merhaba",
              style: TextStyle(color: FixedColor().color2),
            ),
            Text(
                "${Hive.box("logindata").get("isim").toString().toUpperCase()} ${Hive.box("logindata").get("soyisim").toString().toUpperCase()}",
                style: TextStyle(color: FixedColor().color3))
          ],
        ),
        actions: [
          Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: StreamBuilder<List<dynamic>>(
                      stream: listenDocument.duyuruStream(),
                      builder: (BuildContext context,
                          AsyncSnapshot<List<dynamic>> snapshot) {
                        if (snapshot.hasError) {
                          return const Icon(
                            Icons.notifications_none,
                            size: 40,
                          );
                        }

                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return const Icon(
                            Icons.notifications_none,
                            size: 40,
                          );
                        }
                        List<DuyuruModel>? docSnapList =
                            snapshot.data!.cast<DuyuruModel>();
                        List? duyurular =
                            Hive.box("logindata").get("duyurular");
                        List duyurular1=[];
                        Function eq = const ListEquality().equals;
                        bool equals = true;
                        for (var i in docSnapList) {
                          duyurular1.add(i.id);
                        }
                        if (eq(duyurular1, duyurular)) {
                          equals = true;
                        // }else if(duyurular!.length > duyurular1.length){
                          // equals = true;
                        } else {
                          equals = false;
                        }
                        return IconButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.rightToLeft,
                                    duration: const Duration(milliseconds: 500),
                                    child: NotificationPage(
                                      docSnapList: docSnapList,
                                      duyurular1: duyurular1,
                                    )));
                          },
                          icon: const Icon(
                            Icons.notifications_none,
                            size: 40,
                          ),
                          color: equals ? FixedColor().color2 : Colors.red,
                        );
                      }))
        ],
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        shadowColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            FutureBuilder<QuerySnapshot>(
                future: denemeRef,
                builder: (BuildContext context, AsyncSnapshot snapshot1) {
                  if (!snapshot1.hasData) {
                    return const CircularProgressIndicator();
                  } else {
                    List<DocumentSnapshot> docSnapList = snapshot1.data.docs;

                    return Center(
                            child: Container(
                              margin: const EdgeInsets.all(8.0),
                              height:
                                  MediaQuery.of(context).size.height * 0.889,
                              child: ListView.builder(
                                itemCount: docSnapList.length,
                                itemBuilder:
                                    (BuildContext context, int index) {
                                  if (docSnapList[index]['kurumMu'] == true) {
                                    if (docSnapList[index]['aktif'] == true) {
                                      return TrialExamContainer(
                                          kurumMu: true,
                                          buttontext: "SINAVA GİRİŞ",
                                          containertext: docSnapList[index]
                                              ['sinavAdi'],
                                          subtitletext: docSnapList[index]
                                              ['kurumAdi'],
                                          whichExam: docSnapList[index]
                                              ['sinavAdi'],
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.9,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.2);
                                    }
                                  }
                                  if (docSnapList[index]['aktif'] == true) {
                                    return TrialExamContainer(
                                      kurumMu: false,
                                      buttontext: "SINAVA GİRİŞ",
                                      containertext: docSnapList[index]
                                          ['sinavAdi'],
                                      subtitletext: docSnapList[index]
                                          ['kurumAdi'],
                                      whichExam: docSnapList[index]
                                          ['sinavAdi'],
                                      width:
                                          MediaQuery.of(context).size.width *
                                              0.9,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.2,
                                    );
                                  }
                                  return const Center();
                                },
                              ),
                            ),
                          );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
