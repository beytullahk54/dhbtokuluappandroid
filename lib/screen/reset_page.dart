import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbt_okulu/constants/color.dart';
import 'package:dhbt_okulu/model/auth_user_values_provider.dart';
import 'package:dhbt_okulu/screen/register.dart';
import 'package:dhbt_okulu/widgets/custom_dialog_box.dart';
import 'package:dhbt_okulu/widgets/reset_text_form_field.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';


class ResetPage extends StatefulWidget {
  String appbarText;
  ResetPage({Key? key, required this.appbarText }) : super(key: key);

  @override
  _ResetPageState createState() => _ResetPageState();
}

class _ResetPageState extends State<ResetPage> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference kullanici =
      FirebaseFirestore.instance.collection("kullanici");
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.appbarText,
          style: TextStyle(color: FixedColor().color3),
        ),
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
        shadowColor: FixedColor().color1,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  "Şifrenizi Sıfırlayın!",
                  style: TextStyle(fontSize: 30),
                  textAlign: TextAlign.center
                ),
              ),
              ResetTexFormFiel(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Center(
                  child: ElevatedButton(
                    onPressed: () {   
                      
                      ///////////------------------------------->>>>>< reset işlemleri                      
                      customDialogGetter();
                      FirebaseAuth.instance
                          .sendPasswordResetEmail(email: authNotify.value.eposta);
                          Navigator.of(context).pop();                          
                    },
                    child:
                        const Text("E-Posta adresine sıfırlama postası gönder.", style: TextStyle(fontSize: 17)),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      primary: FixedColor().color3,
                      fixedSize: Size(MediaQuery.of(context).size.width * 0.9,
                          MediaQuery.of(context).size.height * 0.055),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Hesabın yok mu?",
                    style: TextStyle(color: FixedColor().color2),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  const Register()));
                    },
                    child: Text(
                      "Kayıt Ol",
                      style: TextStyle(color: FixedColor().color3),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  CustomDialogBox customDialogGetter() { 
    return CustomDialogBox(descriptions: "Şifre Sıfırlama Talebiniz Alınmıştır. E-Posta Adresiniz Üzerinden Kontrol Edebilirsiniz.",title: "Şifre Sıfırlama",text: "Onayla",);
  }

}
