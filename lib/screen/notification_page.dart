import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dhbt_okulu/model/duyuru_model.dart';
import 'package:flutter/material.dart';
import 'package:dhbt_okulu/constants/color.dart';
import 'package:hive/hive.dart';

class NotificationPage extends StatefulWidget {
  List<DuyuruModel>? docSnapList;
  List? duyurular1;
  NotificationPage(
      {Key? key, required this.docSnapList, required this.duyurular1})
      : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  final _firestore = FirebaseFirestore.instance;

  @override
  void initState() {
    // TODO: implement initState
    // for (var duyuru in widget.duyurular1!) {
      
    // }
    Hive.box("logindata").put("duyurular", widget.duyurular1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Query duyuruRef = _firestore.collection("duyurular").orderBy("tarih",descending: true);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Duyurular",
          style: TextStyle(color: FixedColor().color3),
        ),
        centerTitle: true,
        backgroundColor: FixedColor().color1,
        shadowColor: FixedColor().color1,
        iconTheme: IconThemeData(
          color: FixedColor().color3,
        ),
      ),
      body: Container(
        margin: const EdgeInsets.all(8.0),
        height: MediaQuery.of(context).size.height * 0.889,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: const BouncingScrollPhysics(),
          itemCount: widget.docSnapList!.length,
          itemBuilder: (BuildContext context, int index) {
            return _buildNotificationInfoList(widget.docSnapList, index);
          },
        ),
      ),
    );
  }

  Widget _buildNotificationInfoList(List<DuyuruModel>? bundle, int iter) {
    return Card(
      child: ExpansionTile(
        backgroundColor: FixedColor().color1,
        collapsedIconColor: FixedColor().color1,
        collapsedTextColor: FixedColor().color1,
        collapsedBackgroundColor: FixedColor().color3,
        iconColor: FixedColor().color3,
        textColor: FixedColor().color3,
        title: Column(
          children: [
            Text(
              bundle![iter].baslik,
              style:
                  const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
          ],
        ),
        children: <Widget>[
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: SingleChildScrollView(
                  child: Text(
                    bundle[iter].metin,
                    style: TextStyle(
                        fontSize: 12.0,
                        fontWeight: FontWeight.normal,
                        color: FixedColor().color4),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
